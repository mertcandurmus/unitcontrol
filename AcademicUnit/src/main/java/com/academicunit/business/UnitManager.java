package com.academicunit.business;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.academicunit.dataaccess.IUnitDao;
import com.academicunit.entities.Unit;
import com.academicunit.entities.UnitType;


@Service
public class UnitManager implements IUnitService {

	private IUnitDao iUnitDao;
	
	
	@Autowired
	public UnitManager(IUnitDao iUnitDao) {
	
		this.iUnitDao = iUnitDao;
	}

	@Override
	public List<Unit> getAll() {
		return this.iUnitDao.getAll();
	}

	@Override
	@Transactional
	public void add(Unit unit) {
		this.iUnitDao.add(unit);
	}

	@Override
	@Transactional
	public void delete(Unit unit) {
		this.iUnitDao.delete(unit);

	}

	@Override
	@Transactional
	public void update(Unit unit) {
		this.iUnitDao.update(unit);

	}

	@Override
	@Transactional
	public Unit getById(int id) {
		return this.iUnitDao.getById(id);
	}

	@Override
	@Transactional
	public List<Unit> getAttechedUnit(int attachedunit) {
		return this.iUnitDao.getAttechedUnit(attachedunit);
	}

	@Override
	public List<Unit> getByType(String type) {
		return this.iUnitDao.getByType(type);
	}

	@Override
	public List<UnitType> getAllType() {	
			return this.iUnitDao.getAllType();
		
	}


}
