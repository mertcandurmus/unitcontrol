package com.academicunit.entities.audit;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.time.Instant;


@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(
        value = {"cdate", "udate"},
        allowGetters = true
)
public abstract class DateAudit implements Serializable {

    @CreatedDate
    @Column(nullable = false, updatable = false)
    private Instant cdate;

    @LastModifiedDate
    @Column(nullable = false)
    private Instant udate;

	public Instant getCdate() {
		return cdate;
	}

	public void setCdate(Instant cdate) {
		this.cdate = cdate;
	}

	public Instant getUdate() {
		return udate;
	}

	public void setUdate(Instant udate) {
		this.udate = udate;
	}

    
}
