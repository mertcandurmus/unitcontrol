package com.academicunit.entities;

public enum RoleName {
	 ROLE_USER,
	 ROLE_ADMIN
}
