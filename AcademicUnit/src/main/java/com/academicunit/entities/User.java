package com.academicunit.entities;

import com.academicunit.entities.audit.DateAudit;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import org.hibernate.annotations.NaturalId;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

@Entity
@Table(name="users", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "username"
        }),
        @UniqueConstraint(columnNames = {
                "email"
        })
})
public class User extends DateAudit  {

	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
    @NotBlank
    @Size(max = 40)
	private String name;
    
    @NotBlank
    @Size(max = 40)
	private String surname;
	
    @NotBlank
    @Size(max = 15)
	private String username;
	
		@NaturalId
	    @NotBlank
	    @Size(max = 40)
	    @Email
	    private String email;
	
	    @NotBlank
	    @Size(max = 100)
	    private String password;
	
	
	

	@ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
       name="userroles",
       joinColumns={@JoinColumn(name="user_id", referencedColumnName="id")},
       inverseJoinColumns={@JoinColumn(name="role_id", referencedColumnName="id")})
    private Set<Role> roles;
	
    

	public User() {
	}
		
	
	 public User(User user) {
		this.id= user.getId();
        this.name=user.getName();
       this.surname=user.getSurname();
      this.username=user.getUsername();
      this.email=user.getEmail();
       this.password=user.getPassword();
       this.roles=user.getRoles();
	    }


	public User(String name, String surname, String username, String email,
			String password) {

		this.name = name;
		this.surname = surname;
		this.username = username;
		this.email = email;
		this.password = password;
	}




	public User(long id, String name, String surname,
			 String username,  String email,
			 String password, Set<Role> roles) {

		this.id = id;
		this.name = name;
		this.surname = surname;
		this.username = username;
		this.email = email;
		this.password = password;
		this.roles = roles;
	}



	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}

}
