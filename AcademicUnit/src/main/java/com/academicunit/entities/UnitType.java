package com.academicunit.entities;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
//import javax.persistence.OneToOne;
import javax.persistence.Table;



@Entity
@Table(name="unittype")
public class UnitType {

	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	
	@Column(name="name")
	private String namee;
	
	@Column(name="description")
	private String description;
	
	
////    @OneToMany(mappedBy = "unitType", cascade = CascadeType.ALL)
//	@OneToMany(fetch = FetchType.LAZY, mappedBy = "unitType", cascade = CascadeType.ALL)
//    private List<Unit> Unit;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNamee() {
		return namee;
	}

	public void setNamee(String namee) {
		this.namee = namee;
	}

//	public List<Unit> getUnit() {
//		return Unit;
//	}
//
//	public void setUnit(List<Unit> unit) {
//		Unit = unit;
//	}



}
