package com.academicunit.restapi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.academicunit.business.IUnitService;
import com.academicunit.entities.Unit;
import com.academicunit.entities.UnitType;


@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
public class UnitController {

	
	public IUnitService iUnitService;

	@Autowired
	public UnitController(IUnitService iUnitService) {
	
		this.iUnitService = iUnitService;
	}
	
	
	@GetMapping("/units")
	@PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
	public List<Unit> get(){
		
		return this.iUnitService.getAll();
	}
	
	@GetMapping("/types")
	@PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
	public List<UnitType> getTypes(){
		
		return this.iUnitService.getAllType();
	}
	
	
	@PostMapping("/add")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public void add(@RequestBody Unit unitone) {
		this.iUnitService.add(unitone);
	}
	
	
	@PostMapping("/update")		
	@PreAuthorize("hasRole('ROLE_ADMIN')")                   //	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public void update(@RequestBody Unit unitone) {
		this.iUnitService.update(unitone);
	}
	
	@PostMapping("/delete")
	@PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
	public void delete(@RequestBody Unit unitone) {
		this.iUnitService.delete(unitone);
	}
	
	@PostMapping("/delete/{id}")
	@PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
	public void deleteById(@PathVariable int id) {
		Unit unit = this.iUnitService.getById(id);
		this.iUnitService.delete(unit);
	}
	
	@GetMapping("/units/{id}")
	@PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
	public Unit getById(@PathVariable int id) {
		return this.iUnitService.getById(id);
	}
	
	@GetMapping("/unitAttach/{attachedunit}")
	@PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
	public List<Unit> getAttechedUnit(@PathVariable int attachedunit) {
		return this.iUnitService.getAttechedUnit(attachedunit);
	}
	
	
	@GetMapping("/unitType/{type}")
	@PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
	public List<Unit> getAttechedUnit2(@PathVariable String type) {
		return this.iUnitService.getByType(type);
	}
}
