package com.academicunit.dataaccess;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.academicunit.entities.Unit;
import com.academicunit.entities.User;

@Repository
public class HibernateUserDao implements IUserDao {

	
	private EntityManager entityManager;

	@Autowired
	public HibernateUserDao(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	
	@Transactional
	public void add(User user) {
		Session session = entityManager.unwrap(Session.class);
		session.save(user);
	}

	@Transactional
	public void update(User user) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(user);
	}
	
	@Transactional
	public void delete(User user) {
		Session session = entityManager.unwrap(Session.class);
		User deleteUser = session.get(User.class, user.getId());
		session.delete(deleteUser);
	}
	
	
}
