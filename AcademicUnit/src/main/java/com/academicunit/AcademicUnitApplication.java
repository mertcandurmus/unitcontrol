package com.academicunit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AcademicUnitApplication {

	public static void main(String[] args) {
		SpringApplication.run(AcademicUnitApplication.class, args);
	}

}
