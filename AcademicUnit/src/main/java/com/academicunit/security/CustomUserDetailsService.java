package com.academicunit.security;



import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.academicunit.dataaccess.UserRepository;
import com.academicunit.entities.Role;
import com.academicunit.entities.User;


@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;


   
    
	@Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
	   
    	 User user = userRepository.findByUsernameOrEmail(username,username).orElseThrow(() ->
                new UsernameNotFoundException("USer not found with username or email : "+ username)
        );
    	     	 
//    	 if(user != null) {
//             List<GrantedAuthority> authorities = getUserAuthority(user);
//             return buildUserForAuthentication(user, authorities);
//         } else {
//             throw new UsernameNotFoundException("username not found");
//         }

    	 
    	 return UserPrincipal.create(user);
	}  
	
	 private List<GrantedAuthority> getUserAuthority(User user) {
	        List<GrantedAuthority> authorities = user.getRoles().stream().map(role ->
            new SimpleGrantedAuthority(role.getName().name())
    ).collect(Collectors.toList());
	        return authorities;
	    }

	    private UserDetails buildUserForAuthentication(User user, List<GrantedAuthority> authorities) {
	        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), authorities);
	    }

    @Transactional
    public UserDetails loadUserById(Long id) {
        User user = userRepository.findById(id).orElseThrow(
                () -> new UsernameNotFoundException("User not found with id : " + id)
        );

        return UserPrincipal.create(user);
    }
    

}
