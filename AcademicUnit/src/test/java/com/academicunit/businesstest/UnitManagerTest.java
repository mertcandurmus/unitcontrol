package com.academicunit.businesstest;


import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.academicunit.business.IUnitService;
import com.academicunit.business.UnitManager;
import com.academicunit.dataaccess.IUnitDao;
import com.academicunit.entities.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//@RunWith(SpringJUnit4ClassRunner.class)
@RunWith(MockitoJUnitRunner.class)
public class UnitManagerTest {

	@Mock
	private IUnitDao iUnitDao;
	
	@InjectMocks
	private UnitManager unitManager;
	
	
	
	private static final Logger logger = LoggerFactory.getLogger(IUnitService.class);
	
	
	
	@Before
	public void setup(){
	//	MockitoAnnotations.initMocks(this);
	}
	
	
	@Test
	public void shouldReturnAllSamples(){
		
		List<Unit> unitList = new ArrayList<Unit>();
		unitList.add(new Unit(1,"a","a","a","a",1, 0));
		unitList.add(new Unit(2,"a","a","a","a",1, 0));
		unitList.add(new Unit(3,"a","a","a","a",1, 0));
		when(iUnitDao.getAll()).thenReturn(unitList);
		List<Unit> result = unitManager.getAll();
		assertEquals(3, result.size());
		
	}
	
	
	@Test
	public void ShouldReturnOneSampleById(){
		Unit unit =new Unit(1,"a","a","a","a",1, 0);
		when(iUnitDao.getById(1)).thenReturn(unit);
		Unit result =unitManager.getById(1);
		assertEquals(1, result.getId());
		assertEquals("a", result.getType());
		assertEquals("a", result.getName());
		assertEquals("a", result.getOpendate());
		assertEquals("a", result.getRemark());
		assertEquals(1, result.getAttachedunit());
		logger.info(null, status());
		
		
	}
	
	
	@Test
	public void verifyCreateMethod() {
		Unit unit =new Unit(1,"a","a","a","a",1, 0);
		unitManager.add(unit);
		verify(iUnitDao, times(1)).add(unit);
		
		
		
		
	}

	@Test
	public void verifyDeleteMethod(){
		Unit unit =new Unit(1,"a","a","a","a",1, 0);
		unitManager.delete(unit);
        verify(iUnitDao, times(1)).delete(unit);
       
        
	}
	
	
	
	
	@Test
	public void verifyUpdateMethod() {
		Unit unit =new Unit(1,"a","a","a","a",1, 0);
		unitManager.update(unit);
		verify(iUnitDao, times(1)).update(unit);
		

		
		
	}
	
	

	@Test
	public void ShouldReturnOneSampleByAttachId(){
		List<Unit> unitList = new ArrayList<Unit>();
		unitList.add(new Unit(1,"a","a","a","a",1, 0));
		unitList.add(new Unit(2,"a","a","a","a",1, 0));
		unitList.add(new Unit(3,"a","a","a","a",2, 0));
		unitList.add(new Unit(4,"a","a","a","a",3, 0));
		unitList.add(new Unit(5,"a","a","a","a",2, 0));
		unitList.add(new Unit(6,"a","a","a","a",4, 0));
		when(iUnitDao.getAttechedUnit(1)).thenReturn(unitList);
		List<Unit> result = unitManager.getAttechedUnit(1);
		assertEquals(6, result.size());
		
	}
}
